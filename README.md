# Setup Git on your machine

* [Install Git Download](https://gitforwindows.org/)
    * Click through all the default installation options
* Generate your SSH Key Pair
    * Open the application __Git GUI__
    * Go to __*Help*__ > __*Show SSH Key*__
    * Click __*Generate Key*__
    * Input a blank password and confirm it a second time
* Link your bitbucket account to your SSH Key Pair
    * Click __*Copy to Clipboard*__
    * Open Up your [Bitbucket SSH Key Management Page](https://bitbucket.org/account/settings/ssh-keys/)
    * Click __*Add Key*__ and paste in contents of your clipboard
* Configure Git options
    * Replace dummy inputs below with your info
```bash
git config --global user.name "Example Name"
git config --global user.email "example.name@youraspire.com"
git config --system core.longpaths true
```
